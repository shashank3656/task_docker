FROM nginx
ADD https://gitlab.com/shashank3656/task_docker/-/raw/main/index.html /usr/share/nginx/html/
RUN chmod +r /usr/share/nginx/html/index.html
CMD ["nginx", "-g", "daemon off;"]
